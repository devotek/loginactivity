package sharp.teknos.dev.myactivitystack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final EditText userNameEdit = (EditText)findViewById(R.id.usernameEdit);
        final EditText passEdit = (EditText)findViewById(R.id.passwordEdit);
        Button submitBtn = (Button)findViewById(R.id.submitBtn);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Util.USERNAME,userNameEdit.getText().toString());
                intent.putExtra(Util.PASSWORD,passEdit.getText().toString());
                setResult(Util.RESULT_CODE,intent);
                finish();
            }
        });


        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            userNameEdit.setText(bundle.getString(Util.USERNAME));
            passEdit.setText(bundle.getString(Util.PASSWORD));
        }
    }
}
