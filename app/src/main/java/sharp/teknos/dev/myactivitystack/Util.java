package sharp.teknos.dev.myactivitystack;

import android.content.Context;
import android.widget.Toast;

public class Util {

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final int RESULT_CODE = 234;
    public static final int REQUEST_CODE = 235;

    public static void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
