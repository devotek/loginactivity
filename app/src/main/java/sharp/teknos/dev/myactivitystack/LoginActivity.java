package sharp.teknos.dev.myactivitystack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText userNameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userNameEditText = (EditText)findViewById(R.id.usernameEdit);
        passwordEditText = (EditText)findViewById(R.id.passwordEdit);

    }

    public void onBtnPressed(View view){
        if(view.getId() == R.id.loginBtn){
            if (userNameEditText.getText().toString().equals("admin") && passwordEditText.getText().toString().equals("admin")){
                //login success
                Intent intent = new Intent(this,MainActivity.class);

                startActivity(intent);
                finish();

            }else{
                //failed
                //Toast.makeText(this,"Login Failed, Try Again!!!",Toast.LENGTH_LONG).show();
                Util.showToast(this,"Login Failed, Try Again");
            }
        }else if (view.getId() == R.id.regBtn){
            Intent intent = new Intent(this,RegisterActivity.class);
            intent.putExtra(Util.USERNAME,userNameEditText.getText().toString());
            intent.putExtra(Util.PASSWORD,passwordEditText.getText().toString());
            startActivityForResult(intent,Util.REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Util.REQUEST_CODE){
            //

            if (resultCode == Util.RESULT_CODE){
                // you have a bundle
                Bundle bundle = data.getExtras();
                userNameEditText.setText(bundle.getString(Util.USERNAME));
                passwordEditText.setText(bundle.getString(Util.PASSWORD));
            }
        }

    }
}
